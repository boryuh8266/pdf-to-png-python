# PDF-TO-PNG-Python

## Contents
- [Function](#function)
- [Install Requirement Packages](#install-requirement-packages)
- [Jupyter Notebook Format](#jupyter-notebook-format)
- [TODO](#todo)

---

### Function
- Support Folder Input
- Output Format: `$filename_$index.png`

---

### Install Requirement Packages
```shell
pip install -r requirements.txt
```

---

### Jupyter Notebook Format
- main.ipynb

---

### TODO
- [x] Jupyter Notebook Format
- [ ] Python Application With Argv